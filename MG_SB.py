# Give Python access to Blender
import bpy

# Create Panel
class VIEW3D_PT_shader(bpy.types.Panel):
    
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Add Shader"
    bl_category = "Shader Generator"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_idname = "VIEW3D_PT_shader_generator"
    bl_options = {'DEFAULT_CLOSED'}


# Define the layout of the panel
    def draw(self, context):
        layout = self.layout
        
        row = layout.row()
        row.label(text="1. Select an object in your scene.")
        
        row = layout.row()
        row.label(text="2. Choose a shader from the library to apply it.")
        
        self.layout.separator()
        

# Create Sub Panel (Metallics)
        row = layout.row()
        row.label(text="Metallics")
        
        row = layout.row()
        row.operator("shader.silver_operator", text="Silver", icon = "HANDLETYPE_FREE_VEC")
        
        row = layout.row()
        row.operator("shader.golden_operator", text="Golden", icon = "KEYTYPE_KEYFRAME_VEC")
        
        row = layout.row()
        row.operator("shader.bronze_operator", text="Bronze", icon = "KEYTYPE_EXTREME_VEC")
        
        row = layout.row()
        row.operator("shader.brushed_gold_operator", text="Brushed Gold", icon ="COLORSET_09_VEC")
        
        row = layout.row()
        row.operator("shader.brushed_silver_operator", text="Brushed Silver", icon ="COLORSET_13_VEC")
        
        self.layout.separator()
        
# Create Sub Panel (Woods)
        row = layout.row()
        row.label(text="Woods")
        
        row = layout.row()
        row.operator("shader.waved_wood_operator", text="Waved Wood", icon = "MOD_WAVE")
        
        row = layout.row()
        row.operator("shader.varnished_wood_operator", text="Varnished Wood", icon = "BRUSH_DATA")
        
        self.layout.separator()

# Create Sub Panel (Special Materials)
        row = layout.row()
        row.label(text="Special Materials")
        
        row = layout.row()
        row.operator("shader.glass_operator", text="Glass", icon = "COLORSET_04_VEC")
        
        row = layout.row()
        row.operator("shader.jelly_operator", text="Jelly", icon = "COLORSET_06_VEC")
        
        row = layout.row()
        row.operator("shader.polyester_operator", text="Polyester", icon = "SHADING_SOLID")


# SARAH
# Metallics
# Operator for the Silver Shader
class SHADER_OT_SILVER(bpy.types.Operator):
    
    bl_label = "Silver"
    bl_idname = 'shader.silver_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Silver
        material_silver = bpy.data.materials.new(name="Silver")
        
        # Use Nodes for this Material
        material_silver.use_nodes = True
        
        # Create Principled BSDF node
        principled = material_silver.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 1.0  # Metallic
        principled.inputs[2].default_value = 0.200  # Roughness
        principled.inputs[0].default_value = (0.796, 0.808, 0.835, 1)  # Base Color
        
        # Create Material Output node
        material_output = material_silver.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF node to Material Output node
        material_silver.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_silver
        
        return {'FINISHED'}
    


# Operator for the Golden Shader
class SHADER_OT_GOLDEN(bpy.types.Operator):
    
    bl_label = "Golden"
    bl_idname = 'shader.golden_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Golden
        material_golden = bpy.data.materials.new(name="Golden")
        
        # Use Nodes for this Material
        material_golden.use_nodes = True
        
        # Create Principled BSDF node
        principled = material_golden.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 1.0  # Metallic
        principled.inputs[2].default_value = 0.275  # Roughness
        principled.inputs[0].default_value = (1.0, 0.889, 0.233, 1)  # Base Color
        
        # Create Material Output node
        material_output = material_golden.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF node to Material Output node
        material_golden.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_golden
        
        return {'FINISHED'}
    
    
    
#Operator for the Bronze Shader
class SHADER_OT_BRONZE(bpy.types.Operator):
    
    bl_label = "Bronze"
    bl_idname = 'shader.bronze_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Bronze
        material_bronze = bpy.data.materials.new(name="Bronze")
        
        # Use Nodes for this Material
        material_bronze.use_nodes = True
        
        # Create Principled BSDF node
        principled = material_bronze.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 1.0  # Metallic
        principled.inputs[2].default_value = 0.280  # Roughness
        principled.inputs[0].default_value = (0.889, 0.363, 0.176, 1)  # Base Color
        
        # Create Material Output node
        material_output = material_bronze.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF node to Material Output node
        material_bronze.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_bronze
        
        return {'FINISHED'}



# Operator for the Brushed Gold
class SHADER_OT_BRUSHEDGOLD(bpy.types.Operator):
    
    bl_label = "Brushed Gold"
    bl_idname = 'shader.brushed_gold_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Brushed Gold
        material_brushed_gold = bpy.data.materials.new(name="Brushed Gold")
        
        # Use Nodes for this Material
        material_brushed_gold.use_nodes = True
        
        # Create Principled BSDF node
        principled = material_brushed_gold.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 0.9  # Metallic
        principled.inputs[2].default_value = 0.4  # Roughness
        
        # Create Material Output node
        material_output = material_brushed_gold.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF node to Material Output node
        material_brushed_gold.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Create Color Ramp node
        color_ramp_node = material_brushed_gold.node_tree.nodes.new('ShaderNodeValToRGB')
        color_ramp_node.location = (-300, 0)
        color_ramp_node.color_ramp.elements[0].color = (0.283, 0.107, 0.010, 1)
        color_ramp_node.color_ramp.elements[1].color = (0.827, 0.773, 0.627, 1)
        color_ramp_node.color_ramp.elements.new(0.89)
        color_ramp_node.color_ramp.elements[2].color = (0.863, 0.808, 0.670, 1)
        color_ramp_node.color_ramp.elements.new(1)
        color_ramp_node.color_ramp.elements[3].color = (0.859, 0.757, 0.565, 1)
        
        # Connect Color Ramp to Principled BSDF node
        material_brushed_gold.node_tree.links.new(color_ramp_node.outputs[0], principled.inputs[0])
        
        # Create Noise Texture node
        noise_texture_node = material_brushed_gold.node_tree.nodes.new('ShaderNodeTexNoise')
        noise_texture_node.location = (-500, 0)
        noise_texture_node.inputs[2].default_value = 4.0  # Scale
        noise_texture_node.inputs[3].default_value = 2.0  # Detail
        noise_texture_node.inputs[4].default_value = 0.5  # Roughness
        noise_texture_node.inputs[5].default_value = 2.0  # Lacunarity
        noise_texture_node.inputs[6].default_value = 0.0  # Distortion
        
        # Connect Noise Texture node to Color Ramp node
        material_brushed_gold.node_tree.links.new(noise_texture_node.outputs[0], color_ramp_node.inputs[0])
        
        # Create Bump node
        bump_node = material_brushed_gold.node_tree.nodes.new('ShaderNodeBump')
        bump_node.location = (-200, -400)
        bump_node.inputs[0].default_value = 0.1  # Strength
        bump_node.inputs[1].default_value = 1.0  # Distance
        
        # Connect Principled BSDF node to Bump node
        material_brushed_gold.node_tree.links.new(principled.inputs[5], bump_node.outputs[0])
        
        # Create Second Color Ramp node
        second_color_ramp_node = material_brushed_gold.node_tree.nodes.new('ShaderNodeValToRGB')
        second_color_ramp_node.location = (-500, -400)
        second_color_ramp_node.color_ramp.elements[0].color = (0, 0, 0, 1)
        second_color_ramp_node.color_ramp.elements[1].color = (1, 1, 1, 1)
        
        # Connect Second Color Ramp node to Bump node
        material_brushed_gold.node_tree.links.new(second_color_ramp_node.outputs[0], bump_node.inputs[2])
        
        # Create Second Noise Texture node
        second_noise_texture_node = material_brushed_gold.node_tree.nodes.new('ShaderNodeTexNoise')
        second_noise_texture_node.location = (-700, -400)
        second_noise_texture_node.inputs[2].default_value = 600.0  # Scale
        second_noise_texture_node.inputs[3].default_value = 2.0  # Detail
        second_noise_texture_node.inputs[4].default_value = 0.5  # Roughness
        second_noise_texture_node.inputs[5].default_value = 2.0  # Lacunarity
        second_noise_texture_node.inputs[6].default_value = 0.0  # Distortion    
        
        # Connect Second Noise Texture to Second Color Ramp
        material_brushed_gold.node_tree.links.new(second_noise_texture_node.outputs[1], second_color_ramp_node.inputs[0])    
        
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_brushed_gold
        
        return {'FINISHED'}


    
# Operator for the Brushed Silver
class SHADER_OT_BRUSHEDSILVER(bpy.types.Operator):
    
    bl_label = "Brushed Silver"
    bl_idname = 'shader.brushed_silver_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Brushed Silver
        material_brushed_silver = bpy.data.materials.new(name="Brushed Silver")
        
        # Use Nodes for this Material
        material_brushed_silver.use_nodes = True
        
        # Create Principled BSDF node
        principled = material_brushed_silver.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 1.0  # Metallic
        principled.inputs[2].default_value = 0.3  # Roughness
        
        # Create Material Output node
        material_output = material_brushed_silver.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF to Material Output
        material_brushed_silver.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Create Color Ramp node
        color_ramp_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeValToRGB')
        color_ramp_node.location = (-300, 0)
        color_ramp_node.color_ramp.elements[0].color = (0.050, 0.042, 0.037, 1)
        color_ramp_node.color_ramp.elements[1].color = (0.323, 0.309, 0.287, 1)
        color_ramp_node.color_ramp.elements[1].position = (0.477)
        
        # Connect Color Ramp to Principled BSDF
        material_brushed_silver.node_tree.links.new(color_ramp_node.outputs[0], principled.inputs[0])
        
        # Create Noise Texture node
        noise_texture_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeTexNoise')
        noise_texture_node.location = (-500, 0)
        noise_texture_node.inputs[2].default_value = 17.0  # Scale
        noise_texture_node.inputs[3].default_value = 2.0  # Detail
        noise_texture_node.inputs[4].default_value = 0.5  # Roughness
        noise_texture_node.inputs[5].default_value = 2.0  # Lacunarity
        noise_texture_node.inputs[6].default_value = 0.0  # Distortion
        
        # Connect Noise Texture to Color Ramp
        material_brushed_silver.node_tree.links.new(noise_texture_node.outputs[1], color_ramp_node.inputs[0])
        
        # Create Bump node
        bump_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeBump')
        bump_node.location = (-200, -400)
        bump_node.inputs[0].default_value = 0.3  # Strength
        bump_node.inputs[1].default_value = 1.0  # Distance
        
        # Connect Principled BSDF to Bump node
        material_brushed_silver.node_tree.links.new(principled.inputs[5], bump_node.outputs[0])
        
        # Create Mix Color node
        mix_color_node = material_brushed_silver.node_tree.nodes.new("ShaderNodeMixRGB")
        mix_color_node.location = (-400,-600)
        
        # Connect Mix Color node to Bump node
        material_brushed_silver.node_tree.links.new(bump_node.inputs[2], mix_color_node.outputs[0])
        
        # Create Second Color Ramp node
        second_color_ramp_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeValToRGB')
        second_color_ramp_node.location = (-800, -400)
        second_color_ramp_node.color_ramp.elements[0].color = (0.044, 0.037, 0.033, 1)
        second_color_ramp_node.color_ramp.elements[1].color = (0.068, 0.060, 0.053, 1)
        second_color_ramp_node.color_ramp.elements[1].position = (0.400)
                
        # Create Third Color Ramp node
        third_color_ramp_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeValToRGB')
        third_color_ramp_node.location = (-800, -700)
        third_color_ramp_node.color_ramp.elements[0].color = (0.309, 0.292, 0.279, 1)
        third_color_ramp_node.color_ramp.elements[1].color = (0.287, 0.266, 0.250, 1)
        third_color_ramp_node.color_ramp.elements[1].position = (0.400)
        
        # Connect Second Color Ramp to Mix Color node
        material_brushed_silver.node_tree.links.new(second_color_ramp_node.outputs[0], mix_color_node.inputs[1])
        
        # Connect Third Color Ramp to Mix Color node
        material_brushed_silver.node_tree.links.new(third_color_ramp_node.outputs[0], mix_color_node.inputs[2])
        
        # Create Second Noise Texture node
        second_noise_texture_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeTexNoise')
        second_noise_texture_node.location = (-1000, -400)
        second_noise_texture_node.inputs[2].default_value = 17.0  # Scale
        second_noise_texture_node.inputs[3].default_value = 5.0  # Detail
        second_noise_texture_node.inputs[4].default_value = 0.4  # Roughness
        second_noise_texture_node.inputs[5].default_value = 2.0  # Lacunarity
        second_noise_texture_node.inputs[6].default_value = 5.0  # Distortion
        
        # Create Third Noise Texture node
        third_noise_texture_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeTexNoise')
        third_noise_texture_node.location = (-1000, -700)
        third_noise_texture_node.inputs[2].default_value = 3.0  # Scale
        third_noise_texture_node.inputs[3].default_value = 2.0  # Detail
        third_noise_texture_node.inputs[4].default_value = 0.5  # Roughness
        third_noise_texture_node.inputs[5].default_value = 2.0  # Lacunarity
        third_noise_texture_node.inputs[6].default_value = 9.0  # Distortion     
        
        # Connect Second Noise Texture to Second Color Ramp
        material_brushed_silver.node_tree.links.new(second_noise_texture_node.outputs[1], second_color_ramp_node.inputs[0])    
        
        # Connect Third Noise Texture to Third Color Ramp
        material_brushed_silver.node_tree.links.new(third_noise_texture_node.outputs[1], third_color_ramp_node.inputs[0])
                
        # Create Mapping node
        mapping_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeMapping'),
        mapping_node = mapping_node[0]
        mapping_node.location = (-1200, -400)
        
        # Create Texture Coordinate node
        texture_coordinate_node = material_brushed_silver.node_tree.nodes.new('ShaderNodeTexCoord')
        texture_coordinate_node.location = (-1400, -400)
        
        # Connect Second Noise Texture to Mapping node
        material_brushed_silver.node_tree.links.new(second_noise_texture_node.inputs[0], mapping_node.outputs[0])
                
        # Connect Mapping node to Texture Coordinate node
        material_brushed_silver.node_tree.links.new(mapping_node.inputs[0], texture_coordinate_node.outputs[0])
                
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_brushed_silver
        
        return {'FINISHED'}
    

# MANON
# Woods
# Operator for the Waved Wood
class SHADER_OT_WAVEDWOOD(bpy.types.Operator):
    
    bl_label = "Waved Wood"
    bl_idname = 'shader.waved_wood_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Waved Wood
        material_waved_wood = bpy.data.materials.new(name="Waved Wood")
        
        # Use Nodes for this Material
        material_waved_wood.use_nodes = True
        
        # Create Principled BSDF Node
        principled = material_waved_wood.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 0.0  # Metallic
        principled.inputs[2].default_value = 0.400  # Roughness
                
        # Create Material Output Node
        material_output = material_waved_wood.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF node to Material Output node
        material_waved_wood.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Create Color Ramp node
        color_ramp_node = material_waved_wood.node_tree.nodes.new('ShaderNodeValToRGB')
        color_ramp_node.color_ramp.interpolation = 'B_SPLINE'
        color_ramp_node.location = (-300, 0)
        color_ramp_node.color_ramp.elements[0].color = (0.638, 0.533, 0.440, 1)
        color_ramp_node.color_ramp.elements.new(0.323)
        color_ramp_node.color_ramp.elements[1].color = (0.462, 0.366, 0.266, 1)
        color_ramp_node.color_ramp.elements.new(0.366)
        color_ramp_node.color_ramp.elements[2].color = (0.546, 0.418, 0.301, 1)
        color_ramp_node.color_ramp.elements.new(0.557)
        color_ramp_node.color_ramp.elements[3].color = (0.791, 0.716, 0.631, 1)
        color_ramp_node.color_ramp.elements[4].color = (0.574, 0.499, 0.402, 1)
                
        # Connect Color Ramp node to Principled BSDF node
        material_waved_wood.node_tree.links.new(color_ramp_node.outputs[0], principled.inputs[0])
        
        # Create Wave Texture node
        wave_texture_node = material_waved_wood.node_tree.nodes.new('ShaderNodeTexWave')
        wave_texture_node.location = (-500, 0)
        wave_texture_node.inputs[1].default_value = 7.7  # Scale
        wave_texture_node.inputs[2].default_value = 5.1  # Distortion
        wave_texture_node.inputs[3].default_value = 0.4  # Detail
        wave_texture_node.inputs[4].default_value = 1.0  # Detail Scale
        wave_texture_node.inputs[5].default_value = 0.6  # Detail Roughness
        wave_texture_node.inputs[6].default_value = 0.0  # Phase Offset
        wave_texture_node.wave_type = 'RINGS'
        wave_texture_node.rings_direction = 'X'
        wave_texture_node.wave_profile = 'SIN'
                
        # Connect Wave Textude node to Color Ramp node
        material_waved_wood.node_tree.links.new(color_ramp_node.inputs[0], wave_texture_node.outputs[0])
        
        # Create Bump node
        bump_node = material_waved_wood.node_tree.nodes.new('ShaderNodeBump')
        bump_node.location = (-200, -400)
        bump_node.inputs[0].default_value = 0.250  # Strength
        bump_node.inputs[1].default_value = 1.0  # Distance
        
        # Connect Principled BSDF to Bump node
        material_waved_wood.node_tree.links.new(principled.inputs[5], bump_node.outputs[0])
        
        # Create Second Color Ramp node
        second_color_ramp_node = material_waved_wood.node_tree.nodes.new('ShaderNodeValToRGB')
        second_color_ramp_node.location = (-500, -400)
        second_color_ramp_node.color_ramp.elements[0].color = (0, 0, 0, 1)
        second_color_ramp_node.color_ramp.elements[1].color = (1, 1, 1, 1)
        second_color_ramp_node.color_ramp.elements[1].position = (0.380)
        
        # Connect Second Color Ramp to Bump node
        material_waved_wood.node_tree.links.new(second_color_ramp_node.outputs[0], bump_node.inputs[2])
        
        # Create Noise Texture node
        noise_texture_node = material_waved_wood.node_tree.nodes.new('ShaderNodeTexNoise')
        noise_texture_node.location = (-700, -400)
        noise_texture_node.inputs[2].default_value = 17.0  # Scale
        noise_texture_node.inputs[3].default_value = 2.0  # Detail
        noise_texture_node.inputs[4].default_value = 0.5  # Roughness
        noise_texture_node.inputs[5].default_value = 2.0  # Lacunarity
        noise_texture_node.inputs[6].default_value = 0.0  # Distortion
        
        # Connect Noise Texture to Second Color Ramp
        material_waved_wood.node_tree.links.new(noise_texture_node.outputs[1], second_color_ramp_node.inputs[0])
        
        # Create Mapping node
        mapping_node = material_waved_wood.node_tree.nodes.new('ShaderNodeMapping'),
        mapping_node = mapping_node[0]
        mapping_node.location = (-900, -400)
        
        # Create Texture Coordinate node
        texture_coordinate_node = material_waved_wood.node_tree.nodes.new('ShaderNodeTexCoord')
        texture_coordinate_node.location = (-1100, -400)
        
        # Connect Noise Texture to Mapping node
        material_waved_wood.node_tree.links.new(noise_texture_node.inputs[0], mapping_node.outputs[0])
                
        # Connect Mapping node to Texture Coordinate node
        material_waved_wood.node_tree.links.new(mapping_node.inputs[0], texture_coordinate_node.outputs[0])
        
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_waved_wood
        
        return {'FINISHED'}

# Operator for the Varnished Wood
class SHADER_OT_VARNISHEDWOOD(bpy.types.Operator):
    
    bl_label = "Varnished Wood"
    bl_idname = 'shader.varnished_wood_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Varnished Wood
        material_varnished_wood = bpy.data.materials.new(name="Varnished Wood")
        
        # Use Nodes for this Material
        material_varnished_wood.use_nodes = True
        
        # Create Principled BSDF Node
        principled = material_varnished_wood.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 0.0  # Metallic
        principled.inputs[2].default_value = 0.300  # Roughness
                
        # Create Material Output Node
        material_output = material_varnished_wood.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF node to Material Output node
        material_varnished_wood.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Create Color Ramp node
        color_ramp_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeValToRGB')
        color_ramp_node.color_ramp.interpolation = 'LINEAR'
        color_ramp_node.location = (-300, 0)
        color_ramp_node.color_ramp.elements[0].color = (0.048, 0.235, 0.753, 1)
        color_ramp_node.color_ramp.elements[1].color = (0.074, 0.371, 0.880, 1)
        color_ramp_node.color_ramp.elements[1].position = (0.350)
        
        # Connect Color Ramp node to Principled BSDF node
        material_varnished_wood.node_tree.links.new(color_ramp_node.outputs[0], principled.inputs[0])
        
        # Create Noise Texture node
        noise_texture_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeTexNoise')
        noise_texture_node.location = (-500, -0)
        noise_texture_node.inputs[2].default_value = 18.0 # Scale
        noise_texture_node.inputs[3].default_value = 1.75 # Detail
        noise_texture_node.inputs[4].default_value = 0.5  # Roughness
        noise_texture_node.inputs[5].default_value = 2.0  # Lacunarity
        noise_texture_node.inputs[6].default_value = 0.0  # Distortion
        
        # Create Mapping node
        mapping_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeMapping'),
        mapping_node = mapping_node[0]
        mapping_node.location = (-700, -0)
        
        # Create Texture Coordinate node
        texture_coordinate_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeTexCoord')
        texture_coordinate_node.location = (-900, -0)
        
        # Connect Noise Texture node to Mapping node
        material_varnished_wood.node_tree.links.new(noise_texture_node.inputs[0], mapping_node.outputs[0])
                
        # Connect Mapping node to Texture Coordinate node
        material_varnished_wood.node_tree.links.new(mapping_node.inputs[0], texture_coordinate_node.outputs[0])
        
        # Create Bump node
        bump_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeBump')
        bump_node.location = (-200, -400)
        bump_node.inputs[0].default_value = 0.050 # Strength
        bump_node.inputs[1].default_value = 1.0  # Distance
        
        # Connect Principled BSDF node to Bump node
        material_varnished_wood.node_tree.links.new(principled.inputs[5], bump_node.outputs[0])
        
        # Connect Noise Textude node to Color Ramp node
        material_varnished_wood.node_tree.links.new(color_ramp_node.inputs[0], noise_texture_node.outputs[0])
        
        # Create Mix Color node
        mix_color_node = material_varnished_wood.node_tree.nodes.new("ShaderNodeMixRGB")
        mix_color_node.location = (-400,-600)
        
        # Connect Mix Color node to Bump node
        material_varnished_wood.node_tree.links.new(bump_node.inputs[2], mix_color_node.outputs[0])
        
        # Create Second Color Ramp node
        second_color_ramp_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeValToRGB')
        color_ramp_node.color_ramp.interpolation = 'EASE'
        second_color_ramp_node.location = (-800, -400)
        second_color_ramp_node.color_ramp.elements[0].color = (0, 0, 0, 1)
        second_color_ramp_node.color_ramp.elements[1].color = (1, 1, 1, 1)
        second_color_ramp_node.color_ramp.elements[1].position = (0.068)
                
        # Create Third Color Ramp node
        third_color_ramp_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeValToRGB')
        third_color_ramp_node.location = (-800, -700)
        third_color_ramp_node.color_ramp.elements[0].color = (0, 0, 0, 1)
        third_color_ramp_node.color_ramp.elements[1].color = (1, 1, 1, 1)
                
        # Connect Second Color Ramp to Mix Color node
        material_varnished_wood.node_tree.links.new(second_color_ramp_node.outputs[0], mix_color_node.inputs[1])
        
        # Connect Third Color Ramp node to Mix Color node
        material_varnished_wood.node_tree.links.new(third_color_ramp_node.outputs[0], mix_color_node.inputs[2])
        
        # Create Voronoi Texture node
        voronoi_texture_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeTexVoronoi')
        voronoi_texture_node.location = (-1000, -400)
        voronoi_texture_node.inputs[2].default_value = 50.0 # Scale
        voronoi_texture_node.inputs[3].default_value = 0.0  # Detail
        voronoi_texture_node.inputs[4].default_value = 0.5  # Roughness
        voronoi_texture_node.inputs[5].default_value = 2.0  # Lacunarit
        voronoi_texture_node.inputs[6].default_value = 0.0  # Distortion
        voronoi_texture_node.feature = 'DISTANCE_TO_EDGE'
                
        # Create Second Noise Texture node
        second_noise_texture_node = material_varnished_wood.node_tree.nodes.new('ShaderNodeTexNoise')
        second_noise_texture_node.location = (-1000, -700)
        second_noise_texture_node.inputs[2].default_value = 125.0 # Scale
        second_noise_texture_node.inputs[3].default_value = 2.0  # Detail
        second_noise_texture_node.inputs[4].default_value = 0.5  # Roughness
        second_noise_texture_node.inputs[5].default_value = 2.0  # Lacunarity
        second_noise_texture_node.inputs[6].default_value = 0.0  # Distortion
        
        # Connect Voronoi Texture node to Second Color Ramp node
        material_varnished_wood.node_tree.links.new(voronoi_texture_node.outputs[0], second_color_ramp_node.inputs[0])
        
        # Connect Second Noise Texture node to Third Color Ramp node
        material_varnished_wood.node_tree.links.new(second_noise_texture_node.outputs[1], third_color_ramp_node.inputs[0])
               
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_varnished_wood
        
        return {'FINISHED'}
    
    

# Special Materials
# Operator for the Glass
class SHADER_OT_GLASS(bpy.types.Operator):
    
    bl_label = "Glass"
    bl_idname = 'shader.glass_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Glass
        material_glass = bpy.data.materials.new(name="Glass")
        
        # Use Nodes for this Material
        material_glass.use_nodes = True
        
        # Create Glass node
        glass_node = material_glass.node_tree.nodes.new('ShaderNodeBsdfGlass')
        glass_node.location = (0,0)
        glass_node.select = False
        glass_node.inputs[2].default_value = 0.1  # Roughness
                        
        # Create Material Output node
        material_output = material_glass.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Glass node to Material Output node
        material_glass.node_tree.links.new(glass_node.outputs[0], material_output.inputs[0])
        
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_glass
        
        return {'FINISHED'}
    
    
    
# Operator for the Jelly
class SHADER_OT_JELLY(bpy.types.Operator):
    
    bl_label = "Jelly"
    bl_idname = 'shader.jelly_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Jelly
        material_jelly = bpy.data.materials.new(name="Jelly")
        
        # Use Nodes for this Material
        material_jelly.use_nodes = True
        
        # Create Principled BSDF Node
        principled = material_jelly.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 0.0  # Metallic
        principled.inputs[2].default_value = 0.0  # Roughness
        principled.inputs[0].default_value = (0.524, 0.291, 0.661, 1)  # Base Color
        principled.inputs[17].default_value = 1.0 # Transmission
        principled.inputs[26].default_value = (0, 0, 0, 1) # Color Emission
        principled.inputs[27].default_value = 1.0 # Strength
        
        # Create Material Output Node
        material_output = material_jelly.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF node to Material Output node
        material_jelly.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_jelly
        
        return {'FINISHED'}

# Operator for the Polyester
class SHADER_OT_POLYESTER(bpy.types.Operator):
    
    bl_label = "Polyester"
    bl_idname = 'shader.polyester_operator'
    
    def execute(self, context):
        
        # Create a Shader Material and name it Jelly
        material_polyester = bpy.data.materials.new(name="Polyester")
        
        # Use Nodes for this Material
        material_polyester.use_nodes = True
        
        # Create Principled Node
        principled = material_polyester.node_tree.nodes.get('Principled BSDF')
        principled.location = (0,0)
        principled.select = False
        principled.inputs[1].default_value = 0.0  # Metallic
        principled.inputs[2].default_value = 0.418  # Roughness
        principled.inputs[0].default_value = (1, 1, 1, 1)  # Base Color
                
        # Create Output Node
        material_output = material_polyester.node_tree.nodes.get('Material Output')
        material_output.location = (400, 0)
        material_output.select = False
        
        # Connect Principled BSDF node to Material Output node
        material_polyester.node_tree.links.new(principled.outputs[0], material_output.inputs[0])
        
        # Create Bump node
        bump_node = material_polyester.node_tree.nodes.new('ShaderNodeBump')
        bump_node.location = (-200, -400)
        bump_node.inputs[0].default_value = 0.260 # Strength
        bump_node.inputs[1].default_value = 1.0  # Distance
        
        # Connect Bump node to Principled BSDF node
        material_polyester.node_tree.links.new(principled.inputs[5], bump_node.outputs[0])
        
        # Create Multiply node
        multiply_node = material_polyester.node_tree.nodes.new("ShaderNodeMixRGB")
        multiply_node.blend_type = 'MULTIPLY'
        multiply_node.inputs[0].default_value = 0.4 # Fac
        multiply_node.location = (-400,-600)
        
        # Connect Multiply node to Bump node
        material_polyester.node_tree.links.new(bump_node.inputs[2], multiply_node.outputs[0])
        
        # Create Color Ramp node
        color_ramp_node = material_polyester.node_tree.nodes.new('ShaderNodeValToRGB')
        color_ramp_node.color_ramp.interpolation = 'EASE'
        color_ramp_node.location = (-700, -500)
        color_ramp_node.color_ramp.elements[0].color = (0.033, 0.033, 0.033, 1)
        color_ramp_node.color_ramp.elements[0].position = (0.702)
        color_ramp_node.color_ramp.elements[1].color = (0.0, 0.0, 0.0, 1)
        color_ramp_node.color_ramp.elements[1].position = (1.0)
        
        # Create Second Color Ramp node
        second_color_ramp_node = material_polyester.node_tree.nodes.new('ShaderNodeValToRGB')
        color_ramp_node.color_ramp.interpolation = 'LINEAR'
        second_color_ramp_node.location = (-700, -800)
        second_color_ramp_node.color_ramp.elements[0].color = (1, 1, 1, 1)
        second_color_ramp_node.color_ramp.elements[1].color = (1, 1, 1, 1)
                
        # Connect Color Ramp node to Multiply node
        material_polyester.node_tree.links.new(multiply_node.inputs[1], color_ramp_node.outputs[0])
        
        # Connect Second Color Ramp node to Multiply node
        material_polyester.node_tree.links.new(multiply_node.inputs[2], second_color_ramp_node.outputs[0])
        
        # Create Voronoi Texture node
        voronoi_texture_node = material_polyester.node_tree.nodes.new('ShaderNodeTexVoronoi')
        voronoi_texture_node.location = (-900, -500)
        voronoi_texture_node.inputs[1].default_value = 1.0 # W
        voronoi_texture_node.inputs[2].default_value = 27.0 # Scale
        voronoi_texture_node.inputs[3].default_value = 0.0  # Detail
        voronoi_texture_node.inputs[4].default_value = 0.5  # Roughness
        voronoi_texture_node.inputs[5].default_value = 2.0  # Lacunarit
        voronoi_texture_node.inputs[6].default_value = 0.0  # Distortion
        voronoi_texture_node.voronoi_dimensions = '4D'
                
        # Create Wave Texture node
        wave_texture_node = material_polyester.node_tree.nodes.new('ShaderNodeTexWave')
        wave_texture_node.location = (-900, -900)
        wave_texture_node.inputs[1].default_value = 100.7  # Scale
        wave_texture_node.inputs[2].default_value = 0.5  # Distortion
        wave_texture_node.inputs[3].default_value = 16.0  # Detail
        wave_texture_node.inputs[4].default_value = 1.0  # Detail Scale
        wave_texture_node.inputs[5].default_value = 0.5  # Detail Roughness
        wave_texture_node.inputs[6].default_value = 0.0  # Phase Offset
        wave_texture_node.rings_direction = 'Z'
        
        # Connect Voronoi Texture node to Color Ramp node
        material_polyester.node_tree.links.new(voronoi_texture_node.outputs[0], color_ramp_node.inputs[0])
        
        # Connect Wave Texture node to Second Color Ramp node
        material_polyester.node_tree.links.new(wave_texture_node.outputs[0], second_color_ramp_node.inputs[0])
        
        # Adding Material to the currently selected object
        bpy.context.object.active_material = material_polyester
        
        return {'FINISHED'}


def register():
    bpy.utils.register_class(VIEW3D_PT_shader)
    bpy.utils.register_class(SHADER_OT_SILVER)
    bpy.utils.register_class(SHADER_OT_GOLDEN)
    bpy.utils.register_class(SHADER_OT_BRONZE)
    bpy.utils.register_class(SHADER_OT_BRUSHEDGOLD)
    bpy.utils.register_class(SHADER_OT_BRUSHEDSILVER)
    bpy.utils.register_class(SHADER_OT_WAVEDWOOD)
    bpy.utils.register_class(SHADER_OT_VARNISHEDWOOD)
    bpy.utils.register_class(SHADER_OT_GLASS)
    bpy.utils.register_class(SHADER_OT_JELLY)
    bpy.utils.register_class(SHADER_OT_POLYESTER)

def unregister():
    bpy.utils.unregister_class(VIEW3D_PT_shader)
    bpy.utils.unregister_class(SHADER_OT_SILVER)
    bpy.utils.unregister_class(SHADER_OT_GOLDEN)
    bpy.utils.unregister_class(SHADER_OT_BRONZE)
    bpy.utils.unregister_class(SHADER_OT_BRUSHEDGOLD)
    bpy.utils.unregister_class(SHADER_OT_BRUSHEDSILVER)
    bpy.utils.unregister_class(SHADER_OT_WAVEDWOOD)
    bpy.utils.unregister_class(SHADER_OT_VARNISHEDWOOD)
    bpy.utils.unregister_class(SHADER_OT_GLASS)
    bpy.utils.unregister_class(SHADER_OT_JELLY)
    bpy.utils.unregister_class(SHADER_OT_POLYESTER)

register()
